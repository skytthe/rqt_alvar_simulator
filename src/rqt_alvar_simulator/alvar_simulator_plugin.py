#!/usr/bin/env python

import os
import sys

# import QT
from PyQt4 import QtCore

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QLabel, QTreeWidget, QTreeWidgetItem, QVBoxLayout, QCheckBox, QWidget, QToolBar, QLineEdit, QPushButton
from python_qt_binding.QtCore import Qt, QTimer

# import libs
import math
import PyKDL

# import ROS
import rospy
import roslib

# import ROS msgs
from geometry_msgs.msg import Pose, Point, Quaternion
from visualization_msgs.msg import Marker
from ar_track_alvar.msg import AlvarMarker
from ar_track_alvar.msg import AlvarMarkers

# import TF
import tf
import tf.transformations as tfMath
from tf_conversions import posemath

# import
import markers as markers

# constants
pi = math.pi
TF_PUB_FREQ = 10 #[hz]

class AlvarSimulator(Plugin):
   def __init__(self, context):
      # init RQT gui 
      super(AlvarSimulator, self).__init__(context)
      # Give QObjects reasonable names
      self.setObjectName('AlvarSimulator')


      # Process standalone plugin command-line arguments
      from argparse import ArgumentParser
      parser = ArgumentParser()
      # Add argument(s) to the parser.
      parser.add_argument("-q", "--quiet", action="store_true",
                    dest="quiet",
                    help="Put plugin in silent mode")
      args, unknowns = parser.parse_known_args(context.argv())
#        if not args.quiet:
#            print 'arguments: ', args
#            print 'unknowns: ', unknowns

      # Create QWidget
      self._widget = QWidget()
      # Get path to UI file which is a sibling of this file
      # in this example the .ui and .py file are in the same folder
      ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'g.ui')
      # Extend the widget with all attributes and children from UI file
      loadUi(ui_file, self._widget)
      # Give QObjects reasonable names
      self._widget.setObjectName('ARdroneUi')
      # Show _widget.windowTitle on left-top of each plugin (when 
      # it's set in _widget). This is useful when you open multiple 
      # plugins at once. Also if you open multiple instances of your 
      # plugin at once, these lines add number to make it easy to 
      # tell from pane to pane.
      if context.serial_number() > 1:
         self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
      # Add widget to the user interface
      context.add_widget(self._widget)


      '''
      init GUI
      '''
      # ref frame combobox
      self._widget.combobox_root_frame.addItem('ar_marker')

      '''
      init publishers
      '''
      # alvar msg simulator
      self.pubAlvarMsg    = rospy.Publisher('/ar_pose_marker', AlvarMarkers)
      # AR tag marker for visualization
      self.pubArMarker    = rospy.Publisher('/sim_ar_marker', Marker)
      # ARdrone marker for visualization
      self.pubDroneMarker = rospy.Publisher('/sim_ar_drone', Marker)

      '''
      init tf
      '''
      # TF listener
      self.tfl = tf.TransformListener()
      # TF broadcaster
      self.tfb = tf.TransformBroadcaster()      
      #TF broadcaster callback timer 
      self.timer = QtCore.QTimer(self)
      self.timer.timeout.connect(self.tfBroadcasterCallback)
      self.timer.start(1000.0 * 1.0/TF_PUB_FREQ)

   def tfBroadcasterCallback(self):
      # world_sim -> root frame
      root_frame = str(self._widget.combobox_root_frame.currentText()) + "_" + str(self._widget.spinbox_root_id.value())
      world_frame = "sim_world"
      p_world = Point(0.,0.,0.)
      q_world = tf.transformations.quaternion_from_euler(0.,0.,0.)
      self.tfb.sendTransform((p_world.x,p_world.y,p_world.z),
                              q_world,
                              rospy.Time.now(),
                              world_frame,
                              root_frame
                            )


      # publish marker frame
      marker_frame = "sim_marker"
      p_marker = Point(0.,0.,0.)
      q_marker = tf.transformations.quaternion_from_euler(
                              0.,
                              0.,
                              (self._widget.spinbox_marker_yaw.value() * pi)
                           )
      self.tfb.sendTransform((p_marker.x,p_marker.y,p_marker.z),
                             q_marker,
                             rospy.Time.now(),
                             marker_frame,
                             world_frame
                            )                    
      self.pubArMarker.publish(markers.getArMarker(marker_frame,4))
      
      # publish drone frame
      drone_frame = "sim_ARdrone"
      p_drone = Point(self._widget.spinbox_x.value(),
                      self._widget.spinbox_y.value(),
                      self._widget.spinbox_z.value()
                     )
      q_drone = tf.transformations.quaternion_from_euler(
                              (self._widget.spinbox_roll.value()  * pi),
                              (self._widget.spinbox_pitch.value() * pi),
                              (self._widget.spinbox_yaw.value()   * pi),
                           )
      q_drone_stable = tf.transformations.quaternion_from_euler(
                              0,
                              0,
                              (self._widget.spinbox_yaw.value()   * pi),
                           )
      self.tfb.sendTransform((p_drone.x,p_drone.y,p_drone.z),
                             q_drone,
                             rospy.Time.now(),
                             drone_frame,
                             world_frame
                            )
      drone_frame_stable = drone_frame + "_planar"
      self.tfb.sendTransform((p_drone.x,p_drone.y,p_drone.z),
                              q_drone_stable,
                              rospy.Time.now(),
                              drone_frame_stable,
                              world_frame
                              )
      if self._widget.checkBox_drone_vis.isChecked():
         self.pubDroneMarker.publish(markers.getARdroneMarker(drone_frame))

      # generate alvar msg and publish
      if self._widget.checkBox_alvar_msg.isChecked():
         f_drone = PyKDL.Frame(PyKDL.Rotation.Quaternion(*q_drone),
                               PyKDL.Vector(p_drone.x,p_drone.y,p_drone.z))
         f_rot   = PyKDL.Frame(PyKDL.Rotation.RPY(-pi,0,-pi/2),PyKDL.Vector(0.,0.,0.))
         f_alvar = f_rot * f_drone.Inverse()
         pose = posemath.toMsg(f_alvar)
         alvar_marker = AlvarMarker()
         alvar_marker.id = 6
         alvar_marker.pose.pose = pose
         alvar_markers = AlvarMarkers()
         alvar_markers.markers = [alvar_marker]
         self.pubAlvarMsg.publish(alvar_markers) 

   def shutdown_plugin(self): 
      pass

   def save_settings(self, plugin_settings, instance_settings): 
      pass

   def restore_settings(self, plugin_settings, instance_settings):
      pass

   #def trigger_configuration(self):
      # Comment in to signal that the plugin has a way to configure it
      # Usually used to open a configuration dialog

