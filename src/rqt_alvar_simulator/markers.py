#!/usr/bin/env python

# import ROS
import roslib
import rospy
#ROS msgs
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point

def getARdroneMarker(frame_id):
   marker = Marker()
   marker.header.frame_id = frame_id
   marker.header.stamp = rospy.Time.now()
   marker.ns = "ARdroneMesh"
   marker.id = 0
   marker.type = 10
   marker.action = 0
   marker.lifetime = rospy.Duration(1.)
   marker.pose.position.x = 0.05
   marker.pose.position.y = 0.0
   marker.pose.position.z = -0.01
   marker.pose.orientation.x = 0.0
   marker.pose.orientation.y = 0.0
   marker.pose.orientation.z =  0.7071067811865476
   marker.pose.orientation.w = -0.7071067811865476
   marker.scale.x = 0.0006
   marker.scale.y = 0.0006
   marker.scale.z = 0.0006
   marker.color.a = 1.0
   marker.color.r = 0.4
   marker.color.g = 0.4
   marker.color.b = 0.4
   marker.mesh_resource = "package://rqt_alvar_simulator/res/ardrone2.stl";
   return marker

def getArMarker(frame_id, marker_id):
   marker = Marker()
   marker.header.frame_id = frame_id
   marker.header.stamp = rospy.Time.now()
   marker.ns = "alvar_sim_marker"
   marker.id = marker_id
   marker.type = 1
   marker.action = 0
   marker.lifetime = rospy.Duration(1.)
   marker.pose.position.x = 0.0
   marker.pose.position.y = 0.0
   marker.pose.position.z = 0.0
   marker.pose.orientation.x = 0.0
   marker.pose.orientation.y = 0.0
   marker.pose.orientation.z =  0.0
   marker.pose.orientation.w = 1.0
   marker.scale.x = 0.106
   marker.scale.y = 0.106
   marker.scale.z = 0.0212
   marker.color.a = 1.0
   marker.color.r = 0.4
   marker.color.g = 0.4
   marker.color.b = 0.0
   return marker 
   
def getArrow(frame_id, p_start, p_end):
   marker = Marker()
   marker.header.frame_id = frame_id
   marker.header.stamp = rospy.Time.now()
   marker.ns = "arrow"
   marker.id = 0
   marker.type = 0
   marker.action = 0
   marker.lifetime = rospy.Duration(1.)
   marker.scale.x = 0.01
   marker.scale.y = 0.05
   marker.scale.z = 0.1
   marker.color.a = 1.0
   marker.color.r = 1.0
   marker.color.g = 0.0
   marker.color.b = 0.0
   marker.points = [p_start, p_end]
   return marker 
